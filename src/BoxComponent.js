import React from 'react';

export default class BoxComponent extends React.Component {
  wybierzBox = () => {
    this.props.wybierzBox(this.props.rzad, this.props.kolumna);
  };

  render() {
    return (
      <div
        className={this.props.box}
        id={this.props.id}
        onClick={this.wybierzBox}
      />
    );
  }
}

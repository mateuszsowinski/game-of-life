import React from 'react';

import './index.css';
import BoxComponent from './BoxComponent';

export default class GridComponent extends React.Component {
  render() {
    const szerokosc = this.props.kolumny * 14;
    var rzadTab = [];

    var box = '';
    for (var x = 0; x < this.props.rzedy; x++) {
      for (var y = 0; y < this.props.kolumny; y++) {
        let boxId = x + '_' + y;

        box = this.props.siatka[x][y] ? 'box on' : 'box off';
        rzadTab.push(
          <BoxComponent
            box={box}
            key={boxId}
            boxId={boxId}
            rzad={x}
            kolumna={y}
            wybierzBox={this.props.wybierzBox}
          />
        );
      }
    }

    return (
      <div className='grid' style={{ width: szerokosc }}>
        {rzadTab}
      </div>
    );
  }
}

import React from 'react';

import './index.css';
import GridComponent from './GridComponent';
import ButtonComponent from './ButtonComponent';

export default class MainComponent extends React.Component {
  constructor() {
    super();
    //ustawienie wymiarów początkowych całego obszaru
    this.rzedy = 35;
    this.kolumny = 60;
    //ustawienie warunków początkowych
    this.state = {
      pokolenia: 0,
      siatka: Array(this.rzedy)
        .fill()
        .map(() => Array(this.kolumny).fill(false))
    };
  }

  //możliwość ustawienia i wybrania początkowych miejsc rozmieszczenia nasion poprzez klikanie
  wybierzBox = (rzad, kolumna) => {
    let siatkaKopia = tablicaKopia(this.state.siatka);
    siatkaKopia[rzad][kolumna] = !siatkaKopia[rzad][kolumna];
    this.setState({
      siatka: siatkaKopia
    });
  };

  //wygenerowanie początkowej losowej ilości nasion oraz możliwość dodania losowej liczby nasion rozmieszczonych losowo po całym obszarze
  nasiona = () => {
    let siatkaKopia = tablicaKopia(this.state.siatka);
    for (let x = 0; x < this.rzedy; x++) {
      for (let y = 0; y < this.kolumny; y++) {
        if (Math.floor(Math.random() * 4) === 1) {
          siatkaKopia[x][y] = true;
        }
      }
    }
    this.setState({
      siatka: siatkaKopia
    });
  };

  //możliwość ponownego wystartowania życia (dzięki intervalID)
  przyciskStart = () => {
    clearInterval(this.interwalId);
    this.interwalId = setInterval(this.start);
  };

  //możliwość pauzy życia (dzięki intervalID)
  przyciskPauza = () => {
    clearInterval(this.interwalId);
  };

  //czyszczenie siatki oraz wyzerowanie pokoleń
  wyczysc = () => {
    var siatkaPusta = Array(this.rzedy)
      .fill()
      .map(() => Array(this.cols).fill(false));
    this.setState({
      siatka: siatkaPusta,
      pokolenia: 0
    });
  };

  //automatyczny start po uruchomieniu strony
  start = () => {
    let s = this.state.siatka;
    let s2 = tablicaKopia(this.state.siatka);

    //Pętle for powodują sprawdzenie każdego elementu w każdej komórce (każdy rząd i każdą kolumnę)
    for (let x = 0; x < this.rzedy; x++) {
      for (let y = 0; y < this.kolumny; y++) {
        let licz = 0; //liczy ile dane nasiono ma sąsiadów
        //Sprawdzenie wszystkich możliwych warunków (8, bo tyle maksymalnie może być sąsiadów) jakie mogą pojawić się wokół danego nasiona
        if (x > 0) if (s[x - 1][y]) licz++;
        if (x > 0 && y > 0) if (s[x - 1][y - 1]) licz++;
        if (x > 0 && y < this.kolumny - 1) if (s[x - 1][y + 1]) licz++;
        if (y < this.kolumny - 1) if (s[x][y + 1]) licz++;
        if (y > 0) if (s[x][y - 1]) licz++;
        if (x < this.rzedy - 1) if (s[x + 1][y]) licz++;
        if (x < this.rzedy - 1 && y > 0) if (s[x + 1][y - 1]) licz++;
        if (x < this.rzedy - 1 && this.kolumny - 1) if (s[x + 1][y + 1]) licz++;
        if (s[x][y] && (licz < 2 || licz > 3)) s2[x][y] = false; //Zasada 1: Jeśli żyje, liczy ilu sąsiadów przy nim żyje. Jeśli mniej niż 2 lub więcej niż 3, to umiera.
        if (!s[x][y] && licz === 3) s2[x][y] = true; // Zasada 2: Jeśli nie żyje, liczy ilu sąsiadów przy nim żyje. Jeśli 3, to ożywa.
      }
    }
    this.setState({
      siatka: s2,
      pokolenia: this.state.pokolenia + 1
    });
  };

  componentDidMount() {
    this.nasiona();
    this.przyciskStart();
  }

  render() {
    return (
      <div>
        <h1>Gra w życie</h1>
        <ButtonComponent
          przyciskStart={this.przyciskStart}
          przyciskPauza={this.przyciskPauza}
          wyczysc={this.wyczysc}
          nasiona={this.nasiona}
        />
        <GridComponent
          siatka={this.state.siatka}
          rzedy={this.rzedy}
          kolumny={this.kolumny}
          wybierzBox={this.wybierzBox}
        />
        <h2>Pokolenia: {this.state.pokolenia}</h2>
      </div>
    );
  }
}

function tablicaKopia(tab) {
  return JSON.parse(JSON.stringify(tab));
}

import React from 'react';

import './MainComponent';

import { ButtonToolbar } from 'react-bootstrap';

export default class ButtonComponent extends React.Component {
  render() {
    return (
      <div className='buttons'>
        <ButtonToolbar>
          <button
            className='btn btn-default'
            onClick={this.props.przyciskStart}
          >
            Start
          </button>
          <button
            className='btn btn-default'
            onClick={this.props.przyciskPauza}
          >
            Pauza
          </button>
          <button className='btn btn-default' onClick={this.props.wyczysc}>
            Wyczysc
          </button>
          <button className='btn btn-default' onClick={this.props.nasiona}>
            Dodaj nasiona
          </button>
        </ButtonToolbar>
      </div>
    );
  }
}
